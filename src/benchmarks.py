from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from string import ascii_letters
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import tensorflow as tf

def roc(curves, filename):
    """Print and save on file the roc curves:
        curves (list): list of curves, the format for every curve is
                        (name (string), y_test (list), y_pred (list))
        filename (string): name of the file
    """

    for name, y, y_pred in curves:
        fpr, tpr, thresholds = roc_curve(y, y_pred)
        auc_val = auc(fpr, tpr)
        plt.figure(0, figsize=(7, 5))
        plt.xlim(0, 1)
        plt.ylim(0.8, 1)
        plt.plot(fpr, tpr, label='{} ({:.3f})'.format(name, auc_val))
        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.title('ROC curves')
        plt.legend(loc='best')
    plt.savefig(filename, bbox_inches='tight', trasparent=True, pad_inches=0.2)
    plt.show()

def pr(curves, filename):
    """Print and save on file the precision-recall curves:
        curves (list): list of curves, the format for every curve is
                        (name (string), y_test (list), y_pred (list))
        filename (string): name of the file
    """

    for name, y, y_pred in curves:
        prec, rec, thresholds = precision_recall_curve(y, y_pred)
        aupcr_val = auc(rec, prec)
        plt.figure(0, figsize=(7, 5))
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.plot(prec, rec, label='{} ({:.3f})'.format(name, aupcr_val))
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title('Precision-Recall curves')
        plt.legend(loc='best')
    plt.savefig(filename, bbox_inches='tight', trasparent=True, pad_inches=0.2)
    plt.show()

def accuracy(history):
    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

def loss(history):
    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

def correlation(data):
    """Print the correlation matrix:
        data (list): list of data
    """

    sns.set(style="white")
    # Generate a large random dataset
    d = pd.DataFrame(data)
    # Compute the correlation matrix
    corr = d.corr()
    # Generate a mask for the upper triangle
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True
    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(11, 9))
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
                square=True, linewidths=.5, cbar_kws={"shrink": .5})
