import numpy as np
import tensorflow as tf
from keras import backend as K

def recall_acc(y_true, y_pred):
    """Recall metric.
        y_true (list): list with test-set output
        y_pred (list): list with the output predicted
    Only computes a batch-wise average of recall.
    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_acc(y_true, y_pred):
    """Precision metric.
        y_true (list): list with test-set output
        y_pred (list): list with the output predicted
    Only computes a batch-wise average of precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_acc(y_true, y_pred):
    """F1 metric.
        y_true (list): list with test-set output
        y_pred (list): list with the output predicted
    """

    p = precision_acc(y_true, y_pred)
    r = recall_acc(y_true, y_pred)
    return 2*((p*r)/(p+r+K.epsilon()))

def auc_acc(y_true, y_pred):
    """AUC metric.
        y_true (list): list with test-set output
        y_pred (list): list with the output predicted
    """

    auc = tf.metrics.auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return auc
