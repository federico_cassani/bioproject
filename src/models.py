from src.metrics import *
from keras.models import Sequential
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Conv2D, Conv1D
from keras.layers import MaxPooling2D, MaxPooling1D, GlobalAvgPool1D
from keras.layers import Dense
from keras.utils import plot_model
from talos.model.early_stopper import early_stopper
from talos import live
from talos.metrics.keras_metrics import fbeta_score_acc

def cnn1(params):
    """Keras model of a CNN:

    returns:
        model: model of the network
    """

    print('...building the cnn...')
    model = Sequential()

    model.add(Conv1D(filters=64, kernel_size=8, activation='relu', input_shape=(200, 5)))
    model.add(Conv1D(filters=64, kernel_size=8, activation='relu'))
    model.add(Dropout(0.5))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss = 'binary_crossentropy', 
                  optimizer = 'sgd',
                  metrics = ['accuracy'])

    plot_model(model)
    print(model.summary())
    return model

def dense1(input_size):
    """Keras model of a MLP:
        input_size (int): size of the input layer

    returns:
        model: model of the network
    """

    print('...building the mlp...')
    model = Sequential()

    model.add(Dense(256, activation='relu', input_dim=input_size))
    model.add(Dropout(0.2))
    model.add(Dense(256, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss = 'binary_crossentropy', 
                  optimizer = 'sgd',
                  metrics = ['accuracy', auc_acc, f1_acc, precision_acc, recall_acc])

    plot_model(model)
    print(model.summary())
    return model

def dense2(input_size):
    """Keras model of a MLP:
        input_size (int): size of the input layer

    returns:
        model: model of the network
    """

    print('...building the mlp...')
    model = Sequential()

    model.add(Dense(64, activation='relu', input_dim=input_size))
    model.add(Dropout(0.1))
    model.add(Dense(128, activation='softmax'))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='softmax'))
    model.add(Dropout(0.1))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss = 'binary_crossentropy', 
                  optimizer = 'sgd',
                  metrics = ['accuracy', auc_acc, f1_acc, precision_acc, recall_acc])

    plot_model(model)
    print(model.summary())
    return model

def talos_dense1(X_train, y_train, X_val, y_val, params):
    """Talos experiment on a MLP Keras model:
        X_train (list): list of the input training-set
        y_train (list): list of the output trainig-set
        X_val (list): list of the input validation-set
        y_val (list): list of the output validation-set
        params (dict): dict of hyperparameters that talos check

    returns:
        out: result of the training
        model: model of the network
    """

    print('...building the mlp...')
    model = Sequential()
    model.add(Dense(params['neurons0'], activation=params['activation'], input_dim=params['input_size']))
    model.add(Dropout(params['dropout0']))
    model.add(Dense(params['neurons1'], activation= params['activation']))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss = params['losses'], 
                  optimizer = params['optimizer'],
                  metrics = ['accuracy', auc_acc, f1_acc, precision_acc, recall_acc])
    out = model.fit(X_train, y_train, epochs=params['epochs'], batch_size=params['batch_size'], validation_data=(X_val, y_val), verbose=1, callbacks=[early_stopper(params['epochs']), live()])
    return out, model
