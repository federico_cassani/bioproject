import os
import ast
import re
import argparse
from pathlib import Path
import pandas as pd
import pymongo

class DBIndex(object):
    def __init__(self, db_name, host='localhost'):
        self.client = pymongo.MongoClient(host,
                                          username='root',
                                          password='password')
        self.db = self.client[db_name]

if __name__ == '__main__':
    """Load al the dataset from file to mongodb:
        --help to see all the params
    """

    # Instantiate the parser
    parser = argparse.ArgumentParser(description='Import the dataset in mongodb')

    # Arguments
    parser.add_argument('dataset',
                        type = lambda s: Path(s).absolute(),
                        help = 'Path to the dataset folder')
    parser.add_argument('--database',
                        default = 'bio_dataset',
                        help = 'Database name for mongodb')
    args = parser.parse_args()

    database = DBIndex(args.database)
    collections = [database.db['GM12878']]
    collections.append(database.db['HelaS3'])
    collections.append(database.db['HepG2'])
    collections.append(database.db['K562'])

    for col in collections:
        col.drop()

        with \
        open((args.dataset / 'classes/' / col.name).with_suffix('.csv')) as f_classes, \
        open((args.dataset / 'sequences/' / col.name).with_suffix('.fa')) as f_sequences:
            f_epigenomic = pd.read_csv((args.dataset / 'epigenomic-data/' / col.name).with_suffix('.csv')).to_dict(orient='records')

            for line in f_epigenomic:
                name = f_sequences.readline().replace('>', '').rstrip('\n')
                chromosome, start, end = tuple(re.split('[:-]', name))
                chromosome = re.sub('chr', '', chromosome)
                n = line.pop('Unnamed: 0')
                line
                doc = {'name': name,
                       'n:': n,
                       'chr': chromosome,
                       'start': start,
                       'end': end,
                       'label': f_classes.readline().rstrip('\n'),
                       'epigenomic': ast.literal_eval(re.sub(r"'(.*?)':", lambda x: x.group(0).replace('.','_'), repr(line))),
                       'sequence': f_sequences.readline().rstrip('\n').upper()
                       }
                col.insert_one(doc)
