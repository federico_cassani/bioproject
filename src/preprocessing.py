import pymongo
import numpy as np
from random import shuffle
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder, LabelBinarizer, OneHotEncoder
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE, SMOTENC, ADASYN
from imblearn.combine import SMOTEENN, SMOTETomek
from imblearn.under_sampling import NearMiss

## Class wrapper for MongoDB
class DBIndex(object):
    def __init__(self, db_name, host='localhost'):
        self.client = pymongo.MongoClient(host,
                                          username='root',
                                          password='password')
        self.db = self.client[db_name]

def encode(value):
    encoder = LabelEncoder()
    return encoder.fit_transform(value)

def class_encode(label):
    """ Get the binary encoding of the class label:
        label (str): the label to be encoded

    returns:
        int: binary encoding of the label
    """

    class_dict = {'I-E': 0,
                  'I-X': 0,
                  'I-P': 0,
                  'A-E': 1,
                  'A-X': 1,
                  'A-P': 1
                  }
    return class_dict[label]

def class_encode_alt(value):
    """ Get the binary encoding of the class label
        (only for AE vs AP):
        label (str): the label to be encoded

    returns:
        int: binary encoding of the label
    """

    class_dict = {'A-E': 1,
                  'A-P': 0
                  }
    return class_dict[value]

def get_from_db(database, collection, class1):
    """ Get data from MongoDB by label:
        database (str): database name in MongoDB
        collection (str): collection name
        class1 (str): label name

    returns:
        list: the list of the tuple returned by the query
    """

    database = DBIndex(database)
    c = [e for e in database.db[collection].find({'label': class1})]
    return c

def load_data(database, collection, class1, class2, data):
    """ Get data from MongoDB for a particular task and prepare it:
        database (str): database name in MongoDB
        collection (str): collection name
        class1 (str): first label name
        class2 (str): second label name
        data (str): type of the input data ('sequence' or 'epigenomic')

    returns:
        tuple(list, list):  first list represent the input data
                            second list represent the output data
    """

    c1 = get_from_db(database, collection, class1)
    c2 = get_from_db(database, collection, class2)
    dataset = c1 + c2
    shuffle(dataset)
    if data == 'epigenomic':
        X = np.array([list(e[data].values()) for e in dataset])
    elif data == 'sequence':
        X = np.array([encode([e1 for e1 in e[data]]) for e in dataset])
    if (class1 == 'A-E' and class2 == 'A-P') or (class1 == 'A-P' and class2 == 'A-E'):
        y = np.array([class_encode_alt(e['label']) for e in dataset])
    else:
        y = np.array([class_encode(e['label']) for e in dataset])
    return X, y

def splitting(X, y, size):
    """ Split the data in two:
        X (list): list with the input data
        y (list): list with the output data
        size (float): ratio for the split

    returns:
        tuple(list, list, list, list):  first list with the input training-set
                                        second list with the input test-set or validation-set
                                        third list with the output training-set
                                        fourth list with the output test-set or validation-set
    """

    return train_test_split(X, y, test_size=size)

def oversampling_smote(X, y):
    """ Oversampling the data with SMOTE:
        X (list): list with the input data
        y (list): list with the output data

    returns:
        tuple(list, list):  first list with the input data after oversampling
                            second list with the output data after oversampling
    """

    sm = SMOTE(random_state=42)
    X_train, y_train = sm.fit_resample(X, y)
    return X_train, y_train

def oversampling_adasyn(X, y):
    """ Oversampling the data with ADASYN:
        X (list): list with the input data
        y (list): list with the output data

    returns:
        tuple(list, list):  first list with the input data after oversampling
                            second list with the output data after oversampling
    """

    sm = ADASYN(random_state=42)
    X_train, y_train = sm.fit_resample(X, y)
    return X_train, y_train

def undersampling_nearmiss(X, y):
    """ Undersmpling the data with NEARMISS:
        X (list): list with the input data
        y (list): list with the output data

    returns:
        tuple(list, list):  first list with the input data after undersampling
                            second list with the output data after undersampling
    """

    nm = NearMiss()
    X_train, y_train = nm.fit_resample(X, y)
    return X_train, y_train

def sampling_smoteenn(X, y):
    """ Sampling the data with SMOTEENN:
        X (list): list with the input data
        y (list): list with the output data

    returns:
        tuple(list, list):  first list with the input data after sampling
                            second list with the output data after sampling
    """

    sme = SMOTEENN(random_state=42)
    X_train, y_train = sme.fit_resample(X, y)
    return X_train, y_train

def sampling_smotetomek(X, y):
    """ Undersmpling the data with SMOTETOMEK:
        X (list): list with the input data
        y (list): list with the output data

    returns:
        tuple(list, list):  first list with the input data after sampling
                            second list with the output data after sampling
    """

    smt = SMOTETomek(random_state=42)
    X_train, y_train = smt.fit_resample(X, y)
    return X_train, y_train

def one_hot_encode_sequence(train, test, val):
    """ Encode the sequence with OneHotEncoding:
        train (list): list with the input training-set
        test (list): list with the input test-set
        val (list): list with the input validation-set

    returns:
        tuple(list, list, list):    first list with the input training-set encoded
                                    second list with the input test-set encoded
                                    third list with the input validation-set encoded
    """

    encoder = LabelBinarizer()
    encoder.fit([0, 1, 2, 3, 4])

    train = encoder.transform(np.reshape(train, (-1)))
    train = np.reshape(train, (-1, 200, len(encoder.classes_)))
    test = encoder.transform(np.reshape(test, (-1)))
    test = np.reshape(test, (-1, 200, len(encoder.classes_)))
    val = encoder.transform(np.reshape(val, (-1)))
    val = np.reshape(val, (-1, 200, len(encoder.classes_)))
    return train, test, val
