import re
from src.preprocessing import *
from src.benchmarks import *
from src.models import *
import argparse, textwrap
from pathlib import Path
from sklearn.metrics import classification_report

if __name__ == '__main__':
    tasks = {1: ('A-E', 'I-E'),
             2: ('A-P', 'I-P'),
             3: ('', '')}
    # Instantiate the parser
    parser = argparse.ArgumentParser(description='Import the dataset in mongodb',
                                     usage='use "python %(prog)s --help" for more information',
                                     formatter_class=argparse.RawTextHelpFormatter)
    # Arguments
    parser.add_argument('--database',
                        default = 'bio_dataset',
                        help = 'Database name for mongodb')
    parser.add_argument('--epochs',
                        default = 5,
                        type = lambda x: int(x),
                        help = 'Numbers of epochs')
    parser.add_argument('--batch',
                        default = 32,
                        type = lambda x: int(x),
                        help = 'Batch size')
    parser.add_argument('collection',
                        help = 'Collection name (K562, HepG2, HelaS3, GM12878)')
    parser.add_argument('tasks',
                        type = lambda x: tasks[int(x)],
                        help = textwrap.dedent('''\
                            Number of the task that you want to run
                                1. A-E vs I-E
                                2. A-P vs I-P'''))
    parser.add_argument('datatype',
                        help = 'Data type for the input layer (sequence, epigenomic)')
    parser.add_argument('inputsize',
                        type = lambda x: int(x),
                        help = 'Input layer size')
    args = parser.parse_args()

    X, y = load_data(args.database, args.collection, args.tasks[0], args.tasks[1], args.datatype)
    X_train, X_test, y_train, y_test = splitting(X, y, 0.3)
    X_train, X_val, y_train, y_val = splitting(X_train, y_train, 0.3)
    X_train, y_train = oversampling_smote(X_train, y_train)
    #X_train, X_test, X_val = one_hot_encode_sequence(X_train, X_test, X_val)

    print('...training...')
    model = dense1(args.inputsize)
    history = model.fit(X_train, y_train, epochs=args.epochs, batch_size=args.batch, validation_data=(X_val, y_val), verbose=1)

    print('...benchamks...')

    nl = list(zip(X_test, y_test))
    zerox, zeroy = zip(*[(x,y) for (x,y) in nl if y == 0])
    onex, oney  = zip(*[(x,y) for (x,y) in nl if y == 1])
    scores = model.evaluate(X_test, y_test, batch_size=32, verbose=0)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    scores1 = model.evaluate(np.array(zerox), np.array(zeroy), batch_size=32, verbose=0)
    print("%s: %.2f%%" % (model.metrics_names[1], scores1[1]*100))
    scores2 = model.evaluate(np.array(onex), np.array(oney), batch_size=32, verbose=0)
    print("%s: %.2f%%" % (model.metrics_names[1], scores2[1]*100))

    y_pred = model.predict(X_test)
    roc([("prova", y_test, y_pred)])
    accuracy(history)
    loss(history)
